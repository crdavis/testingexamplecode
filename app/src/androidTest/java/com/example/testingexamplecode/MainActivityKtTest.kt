package com.example.testingexamplecode

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertTextContains
import androidx.compose.ui.test.hasParent
import androidx.compose.ui.test.hasTestTag
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.runner.RunWith
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import com.example.testingexamplecode.ui.theme.TestingExampleCodeTheme
import org.junit.Before
import org.junit.Test

//Run with JUnit test runner provided by Android Testing Framework
@RunWith(AndroidJUnit4::class)
class MainActivityKtTest {

    //Establish a Compose rule to get access to the composable component
    @get: Rule
    val composeTestRule = createComposeRule()

    //Set up the content before running each test
    @Before
    fun setUP() {
        composeTestRule.setContent {
            TestingExampleCodeTheme {
                MyScreen()  //Set the composable to be tested
            }
        }
    }

    @Test
    fun buttonAndTextAreDisplayed() {
        // Check if the button and the click count text are displayed
        composeTestRule.onNodeWithTag("MyButton").assertExists().assertIsDisplayed()
        composeTestRule.onNodeWithTag("ClickCountText").assertExists().assertIsDisplayed()
    }

    @Test
    fun buttonClickIncrementsCount() {
        // Perform a click and verify that the text is updated
        composeTestRule.onNodeWithTag("MyButton").performClick()
        composeTestRule.onNodeWithTag("ClickCountText").assertTextContains("Click count: 1")

        // Perform another click and verify that the text is updated again
        composeTestRule.onNodeWithTag("MyButton").performClick()
        composeTestRule.onNodeWithTag("ClickCountText").assertTextContains("Click count: 2")
    }

    @Test
    fun hasParentCheck() {
        // Verify that the button has a parent with the test tag "MainColumn"
        composeTestRule.onNode(
            hasParent(hasTestTag("MainColumn"))
                .and(hasTestTag("MyButton"))
        ).assertExists().assertIsDisplayed()
    }
}